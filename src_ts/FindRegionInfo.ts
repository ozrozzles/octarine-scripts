import { Color, DOTAGameUIState_t, EventsSDK, GameRules, GameState, Menu as MenuSDK, Parse, RendererSDK, Vector2 } from "./wrapper/Imports"

const Menu = MenuSDK.AddEntryDeep(["Main", "Finding Info"])
const State = Menu.AddToggle("State", false)
const TextPosition = Menu.AddVector2("Position", new Vector2(1, 7), new Vector2(1, 1), new Vector2(70, 70))
const TextSize = Menu.AddSlider("Text size", 14, 12, 48)
const BoldText = Menu.AddToggle("Bold text", true)

const kv = Parse.parseKVFile("scripts/matchgroups.txt")

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Finding Info", "Информация о поиске"],
]))

let UpdateDataRegion: Nullable<RecursiveMap>

function LoadMatchGroups(): Map<number, string> {
	const values = [...((kv.get("matchgroups") as RecursiveMap) ?? new Map()).values()]
		.filter(a => a instanceof Map) as RecursiveMap[]
	return new Map(values.map(a => [parseInt(a.get("group") as string), a.get("channel") as string]))
}

EventsSDK.on("Draw", () => {
	if (UpdateDataRegion === undefined
		|| !State.value
		|| GameState.UIState === DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME
		|| GameRules?.IsInGame
	)
		return

	const group_finder = UpdateDataRegion.get("legacy_searching_players_by_group_source2") as number[]

	let current_id = 0
	RendererSDK.Text(
		`Finding match player counts:`,
		RendererSDK.WindowSize.Clone()
			.DivideScalarForThis(100)
			.MultiplyForThis(TextPosition.Vector)
			.AddScalarY(current_id++ * TextSize.value),
		Color.White,
		"Calibri",
		TextSize.value,
		BoldText.value ? 700 : 400,
	)
	LoadMatchGroups().forEach((name_server, key) => {
		const player_count = group_finder[key] ?? 0
		if (player_count === 0)
			return

		RendererSDK.Text(
			`${name_server.replace("Perfect World", "PW")}: ${player_count} players`,
			RendererSDK.WindowSize.Clone()
				.DivideScalarForThis(100)
				.MultiplyForThis(TextPosition.Vector)
				.AddScalarY(current_id++ * TextSize.value),
			Color.White,
			"Calibri",
			TextSize.value,
			BoldText.value ? 700 : 400,
		)
	})
})

EventsSDK.on("MatchmakingStatsUpdated", data => UpdateDataRegion = data)
