import { Color, Menu, Notification, Rectangle, RendererSDK, Vector2 } from "wrapper/Imports"
import { MessageNotify, ReloadOnClickState } from "../menu"

declare global {
	const reload: () => void
}

export class ScriptsUpdated extends Notification {
	constructor(public PathIcon: string) {
		super({
			timeToShow: 6 * 1000,
		})
	}

	public OnClick(): boolean {
		if (!ReloadOnClickState.value)
			return false
		reload()
		return true
	}

	public Draw(position: Rectangle): void {
		const opacity = this.Opacity
		const Position = this.GetPosition(position)
		const RightPosition = this.GetRightPosition(position, Position)
		const centerPosition = this.GetCenterPosition(position, Position, RightPosition)
		const opacityWhite = Color.White.SetA(opacity)
		const opacityInfo = new Color(104, 4, 255).SetA(opacity)

		RendererSDK.Image(`${this.PathIcon}/menu/background_inactive.svg`, position.pos1, -1, position.Size, opacityWhite)
		RendererSDK.Image(`${this.PathIcon}/menu/icons/info.svg`, Position.pos1, -1, Position.Size, opacityInfo)

		let Text: string
		let language_num = 0

		switch (Menu.Localization.SelectedUnitName) {
			case "russian":
				language_num = 1
				Text = MessageNotify.RU
				break
			default:
				language_num = 0
				Text = MessageNotify.EN
				break
		}

		if (opacity <= 70)
			return

		const TextSize = RendererSDK.GetTextSize(Text, "Calibri", centerPosition.Height / 2).x
		const CenterX = language_num === 1
			? centerPosition.x + Position.Height + (Position.Width / 1.55)
			: centerPosition.x + (Position.Height / 1.05)
		const CenterY = centerPosition.y + (centerPosition.Height / 2.90)
		const Vector = new Vector2(CenterX, CenterY).AddScalarX((centerPosition.Width - TextSize) / 2)

		RendererSDK.Text(
			Text,
			Vector,
			Color.White.SetA(opacity),
			"Calibri",
			centerPosition.Height / 3,
		)
	}

	private GetPosition(position: Rectangle) {
		const result = position.Clone()
		result.x += position.Width * 0.05
		result.y += position.Height * 0.25
		result.Width = position.Width * 0.15
		result.Height = position.Height * 0.5
		return result
	}

	private GetRightPosition(position: Rectangle, position1: Rectangle) {
		const result = position1.Clone()
		result.Width *= 0.75
		result.x = position.pos2.x - position.Width * 0.05 - result.Width
		return result
	}

	private GetCenterPosition(position: Rectangle, position1: Rectangle, position2: Rectangle) {
		const result = position.Clone()
		result.Width = position.Width * 0.18
		result.Height = position.Height * 0.6
		result.x = (position1.pos2.x + position2.x) / 2 - result.Width / 2
		result.y += (position.Height - result.Height) / 2
		return result
	}
}
