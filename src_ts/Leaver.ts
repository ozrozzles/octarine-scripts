import { EventsSDK, GameState, Menu, Parse, PlayerResource, Team } from "wrapper/Imports"

const tree = Menu.AddEntryDeep(["Utility", "Bait leave"])
const autodisconnect = tree.AddToggle("Auto Disconnect")
const Additionaldelay = tree.AddSlider("Delay auto disconnect", 1, 1, 10)
const playersList = tree.AddDropdown("Player", [
	"Radiant, 1",
	"Radiant, 2",
	"Radiant, 3",
	"Radiant, 4",
	"Radiant, 5",
	"Dire, 1",
	"Dire, 2",
	"Dire, 3",
	"Dire, 4",
	"Dire, 5",
])
const Language = tree.AddDropdown("Language", ["Russian", "English"])
const button = tree.AddButton("Leave button")
const heroes = [...(Parse.parseKVFile("scripts/npc/npc_heroes.txt").get("DOTAHeroes") as RecursiveMap).values()].filter(a => a instanceof Map) as RecursiveMap[]

let dc_time = 0
const gap = "<br>".repeat(75)
const colors = ["#415fff", "#83ffda", "#c3009c", "#d5ff16", "#f16900", "#ff6ca5", "#85c83b", "#74d6f9", "#009e31", "#8f6f00"]

button.OnValue(() => {
	if (PlayerResource === undefined)
		return

	let PlayerID = playersList.selected_id
	for (let i = 0; i < 10; i++)
		if (
			PlayerResource.PlayerTeamData[i].TeamSlot === PlayerID % 5
			&& PlayerResource.PlayerData[i].Team === (
				PlayerID < 5
					? Team.Radiant
					: Team.Dire
			)
		) {
			PlayerID = i
			break
		}
	PlayerResource.PlayerTeamData.find(data => data.TeamSlot === (playersList.selected_id % 5))
	const PlayerName = PlayerResource.PlayerData[PlayerID]?.Name ?? ""
	const PlayerHeroID = (PlayerResource.PlayerTeamData[PlayerID]?.SelectedHeroID ?? 0).toString()

	let switch_language = ""

	const PlayerHero = heroes.find(hero => (hero.get("HeroID") as string) === PlayerHeroID)?.get("workshop_guide_name") ?? ""
	switch (Language.selected_id) {
		case 0:
			switch_language = `
<font color="${colors[PlayerID]}">${PlayerName} (${PlayerHero})</font> отключается от игры. Пожалуйста, дождитесь повторного подключения.<br><font color='#FF0000'>
<b>У <font color="${colors[PlayerID]}">${PlayerName} (${PlayerHero})</font> осталось 5 мин. для повторного подключения.</b></font>
<br> <font color="${colors[PlayerID]}">${PlayerName} (${PlayerHero})</font> покидает игру.<br><font color='#00FF00'><b>Теперь эту игру можно спокойно покинуть.</b></font>`
			break
		case 1:
			switch_language = `
<font color="${colors[PlayerID]}">${PlayerName} (${PlayerHero})</font> has disconnected from the game. Please wait for them to reconnect.<br><font color='#FF0000'>
<b> <font color="${colors[PlayerID]}">${PlayerName} (${PlayerHero})</font> has 5 minutes left to reconnect.</b></font>
<br> <font color="${colors[PlayerID]}">${PlayerName} (${PlayerHero})</font> has abandoned the game.<br><font color='#00FF00'><b>This game is now safe to leave.</b></font>`
			break
	}

	ChatWheelAbuse(gap + switch_language)

	if (autodisconnect.value)
		dc_time = hrtime()
})

EventsSDK.on("Draw", () => {
	if (dc_time === 0 || dc_time + Additionaldelay.value * 1000 > hrtime())
		return
	GameState.ExecuteCommand("disconnect")
	dc_time = 0
})
