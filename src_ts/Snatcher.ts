import {
	ArrayExtensions,
	Color,
	Courier,
	DOTAGameUIState_t,
	DOTA_RUNES,
	EntityManager,
	EventsSDK,
	GameRules,
	GameSleeper,
	GameState,
	Hero,
	LocalPlayer,
	Menu,
	npc_dota_hero_meepo,
	Particle,
	ParticleAttachment_t,
	ParticlesSDK,
	PhysicalItem,
	RendererSDK,
	Rune,
	SpiritBear,
	Unit,
	Vector3,
} from "wrapper/Imports"
import { GameX } from "./X-Core/Imports"

enum ESelectedType {
	ALL = 0,
	ONLY_BOUNTY = 1,
	ONLY_POWER = 2,
}

let selectedRuneType: ESelectedType = ESelectedType.ALL

const particleManager = new ParticlesSDK(),
	gameSleeper = new GameSleeper(),
	allRunesParticles = new Map<Rune, Particle[]>(),
	picking_up = new Map<Unit, Rune | PhysicalItem>()

const snatcherMenu = Menu.AddEntryDeep(["Utility", "Snatcher"])

const stateMain = snatcherMenu.AddToggle("State", true)
stateMain.OnValue(() => destroyRuneAllParticles())

// ----- Rune

const runeMenu = snatcherMenu.AddNode("Rune settings")

const stateRune = runeMenu.AddToggle("Snatch Rune", true)
stateRune.OnDeactivate(destroyRuneAllParticles)

const pickupRange = 150

const typesSelect = runeMenu.AddDropdown("Runes to snatch", ["All", "Bounty", "PowerUps"])
typesSelect.OnValue(self => selectedRuneType = self.selected_id)

runeMenu.AddKeybind("Rune toogle").OnRelease(() => stateRune.value = !stateRune.value)

const runeHoldKey = runeMenu.AddKeybind("Rune hold key")
runeHoldKey.OnRelease(() => !stateRune.value && destroyRuneAllParticles())

// -- Draw particles

const drawParticles = runeMenu.AddNode("Draw indicators (particles)")

const drawParticleTake = drawParticles.AddToggle("Take rune")
drawParticleTake.OnValue(destroyRuneAllParticles)

const drawParticleTake_Color = drawParticles.AddColorPicker("indicators color")
drawParticleTake_Color.OnValue(updateRuneAllParticle)

// ----- Items

const itemsMenu = snatcherMenu.AddNode("Items settings")

const stateItems = itemsMenu.AddToggle("Snatch Items", true)

itemsMenu.AddKeybind("Items toogle").OnRelease(() => stateItems.value = !stateItems.value)

const itemsHoldKey = itemsMenu.AddKeybind("Items hold key")
itemsHoldKey.OnRelease(() => !stateItems.value)

const listOfItems = itemsMenu.AddImageSelector("Items for snatch", [
	"item_gem",
	"item_cheese",
	"item_rapier",
	"item_aegis",
	"item_refresher_shard",
	"item_ultimate_scepter_2",
	"item_aghanims_shard",
], new Map<string, boolean>([
	["item_gem", true],
	["item_cheese", true],
	["item_rapier", true],
	["item_aegis", true],
	["item_refresher_shard", true],
	["item_ultimate_scepter_2", true],
	["item_aghanims_shard", true],
]))

const stateControllables = snatcherMenu.AddToggle("Use other units")

// ----- Draw

const drawMenu = snatcherMenu.AddNode("Draw")
const drawStatus = drawMenu.AddToggle("Draw status"),
	statusPosX = drawMenu.AddSlider("Position X (%)", 0, 0, 100),
	statusPosY = drawMenu.AddSlider("Position Y (%)", 75, 0, 100)

EventsSDK.on("GameEnded", () => {
	picking_up.clear()
	gameSleeper.FullReset()
})

EventsSDK.on("EntityDestroyed", ent => {
	if ((ent instanceof Rune) || (ent instanceof PhysicalItem))
		removedIDRune(ent)
	if (ent instanceof Unit)
		picking_up.delete(ent)
})

EventsSDK.on("Tick", () => {
	if (!stateMain.value)
		return false

	const controllables: Unit[] = stateControllables.value
		? GetControllables()
		: LocalPlayer!.Hero !== undefined ? [LocalPlayer!.Hero] : []

	snatchRunes(controllables)
	snatchItems(controllables)
})

EventsSDK.on("Draw", () => {
	if (!drawStatus.value || !GameRules?.IsInGame || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return

	let text = ""

	// rune
	text += `${stateRune.InternalName}: ${(stateRune.value || runeHoldKey.is_pressed) ? "On" : "Off"}`

	text += " | "

	// items
	text += `${stateItems.InternalName}: ${(stateItems.value || itemsHoldKey.is_pressed) ? "On" : "Off"}`

	RendererSDK.Text(text, RendererSDK.WindowSize.DivideScalar(100).MultiplyScalarX(statusPosX.value).MultiplyScalarY(statusPosY.value))
})

function GetControllables() {
	return EntityManager.GetEntitiesByClass(Unit).filter(npc =>
		(npc instanceof Hero || npc instanceof SpiritBear || npc instanceof Courier)
		&& !npc.IsIllusion
		&& npc.IsControllable
		&& !npc.HasBuffByName("modifier_spirit_breaker_charge_of_darkness"),
	)
}

// ------- Rune

function snatchRunes(controllables: Unit[]) {
	if (!stateRune.value && !runeHoldKey.is_pressed)
		return
	EntityManager.GetEntitiesByClass(Rune).forEach(rune => {
		if (selectedRuneType === ESelectedType.ONLY_BOUNTY && rune.Type !== DOTA_RUNES.DOTA_RUNE_BOUNTY)
			return
		if (selectedRuneType === ESelectedType.ONLY_POWER && rune.Type === DOTA_RUNES.DOTA_RUNE_BOUNTY)
			return
		const near = ArrayExtensions.orderBy(controllables, unit => unit.Distance(rune)).some(npc => snatchRuneByUnit(npc, rune))
		if (!near && drawParticleTake.value)
			destroyRuneParticles(rune)
	})
}

function snatchRuneByUnit(npc: Unit, rune: Rune): boolean {
	if ((picking_up.has(npc) && picking_up.get(npc) !== rune) || npc instanceof Courier || !rune.IsVisible)
		return false

	if (!npc.IsStunned && !npc.IsWaitingToSpawn && npc.IsAlive) {
		const Distance = npc.Distance2D(rune)
		if (Distance <= pickupRange && !(npc.IsInvulnerable && Distance > 100)) {
			picking_up.set(npc, rune)
			npc.PickupRune(rune)
			return false
		}
		if (drawParticleTake.value && !allRunesParticles.has(rune)) {
			allRunesParticles.set(rune, [])
			createRuneParticle(rune, new Color(0, 255), pickupRange)
		}
	}
	return true
}

function removedIDRune(ent: Rune | PhysicalItem) {
	for (const [unit, ent_] of picking_up.entries())
		if (ent === ent_) {
			picking_up.delete(unit)
			break
		}
	if (ent instanceof Rune)
		destroyRuneParticles(ent)
}

function createRuneParticle(ent: Rune, color: Color, radius: number) {
	const ar = allRunesParticles.get(ent)!
	ar.push(particleManager.AddOrUpdate(
		`Rune_${ent.Index}_${ar.length}`,
		"particles/ui_mouseactions/drag_selected_ring.vpcf",
		ParticleAttachment_t.PATTACH_ABSORIGIN,
		ent,
		[1, color],
		[2, new Vector3(radius * 1.1, 255)],
	))
}

function updateRuneAllParticle() {
	allRunesParticles.forEach(partcl => partcl[0].SetControlPoint(1, drawParticleTake_Color.selected_color))
}

function destroyRuneParticles(rune: Rune) {
	const particles = allRunesParticles.get(rune)
	if (particles === undefined)
		return

	particles.forEach(particleID => particleID.Destroy())
	allRunesParticles.delete(rune)
}

function destroyRuneAllParticles() {
	particleManager.DestroyAll()
	allRunesParticles.clear()
}

// ------- Items

function snatchItems(controllables: Unit[]) {
	if ((!stateItems.value && !itemsHoldKey.is_pressed) || listOfItems.IsZeroSelected)
		return

	const free_controllables = controllables
	EntityManager.GetEntitiesByClass(PhysicalItem).forEach(item => {
		const m_hItem = item.Item
		if (m_hItem === undefined || !listOfItems.IsEnabled(m_hItem.Name))
			return

		const itemOwner = m_hItem.Owner
		const can_take_backpack = m_hItem.Name !== "item_aegis" && m_hItem.Name !== "item_gem" && m_hItem.Name !== "item_rapier"

		free_controllables.some((npc, index) => {
			if (
				itemOwner === npc
				|| !npc.IsAlive
				|| !npc.IsInRange(item, pickupRange)
				|| (npc instanceof npc_dota_hero_meepo && npc.IsClone)
				|| (npc instanceof Courier && (m_hItem.Name === "item_aegis" || m_hItem.Name === "item_rapier"))
				|| !(npc.Inventory.HasFreeSlotsInventory || (can_take_backpack && npc.Inventory.HasFreeSlotsBackpack))
				|| (picking_up.has(npc) && picking_up.get(npc) !== item)
				|| gameSleeper.Sleeping(`PickUpItems_${item.Index}`)
			)
				return false

			picking_up.set(npc, item)
			npc.PickupItem(item)
			gameSleeper.Sleep(100 + (GameX.Ping / 1000), `PickUpItems_${item.Index}`)
			free_controllables.splice(index, 1)
			return true
		})
	})
}
