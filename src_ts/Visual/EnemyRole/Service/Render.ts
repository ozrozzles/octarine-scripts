import { Color, GUIInfo, LaneSelectionFlags_t, LocalPlayer, Menu, RendererSDK, Team, Vector2 } from "wrapper/Imports"
import { PathX } from "X-Core/Imports"
import { RolesDrawMode, RolesTextSize } from "../menu"

export class DRAW_ROLES {
	public static Roles(
		index: number,
		role: LaneSelectionFlags_t,
	) {
		if (index >= 5)
			return
		const position = LocalPlayer?.Team === Team.Dire
			? GUIInfo.PreGame.RadiantPlayersRoles[index]
			: GUIInfo.PreGame.DirePlayersRoles[index]
		let role_str: string
		let image_role: string

		switch (role) {
			case LaneSelectionFlags_t.HARD_SUPPORT:
				image_role = PathX.Images.hardsupport
				role_str = `(5) ${Menu.Localization.Localize("Support")}`
				break
			case LaneSelectionFlags_t.MID_LANE:
				image_role = PathX.Images.midlane
				role_str = Menu.Localization.Localize("Mid Lane")
				break
			case LaneSelectionFlags_t.OFF_LANE:
				image_role = PathX.Images.offlane
				role_str = Menu.Localization.Localize("Off Lane")
				break
			case LaneSelectionFlags_t.SAFE_LANE:
				image_role = PathX.Images.safelane
				role_str = Menu.Localization.Localize("Safe Lane")
				break
			case LaneSelectionFlags_t.SOFT_SUPPORT:
				image_role = PathX.Images.softsupport
				role_str = `(4) ${Menu.Localization.Localize("Support")}`
				break
			default:
				return
		}

		switch (RolesDrawMode.selected_id) {
			case 0:
				RendererSDK.Image(
					image_role,
					new Vector2(
						position.x + (position.Width - DRAW_ROLES.RoleIconSize.x) / 2,
						position.y + (position.Height - DRAW_ROLES.RoleIconSize.y) / 2,
					),
					-1,
					DRAW_ROLES.RoleIconSize,
				)
				break
			case 1: {
				const font_size = GUIInfo.ScaleHeight(RolesTextSize.value, RendererSDK.WindowSize)
				const text_size = RendererSDK.GetTextSize(role_str, "Calibri", font_size)
				RendererSDK.Text(
					role_str,
					position.pos1.Clone()
						.AddScalarX((position.Width - text_size.x) / 2)
						.AddScalarY((position.Height - text_size.y)),
					DRAW_ROLES.RoleTextColor,
					"Calibri",
					font_size,
				)
				break
			}
		}
	}
	private static readonly RoleIconSize = new Vector2(32, 32)
	private static readonly RoleTextColor = new Color(0xAA, 0xAA, 0xAA)
}
