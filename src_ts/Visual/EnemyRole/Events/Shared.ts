import { EventsSDK, LaneSelectionFlags_t } from "wrapper/Imports"
import { CSODOTALobby_State, ROLES_DATA } from "../data"

EventsSDK.on("SharedObjectChanged", (id, _, obj) => {
	if (id !== 2004)
		return

	if (obj.get("state") === CSODOTALobby_State.NOTREADY) {
		ROLES_DATA.Roles[0] = ROLES_DATA.Roles[1] = []
		return
	}

	const members = obj.get("all_members") as RecursiveMap[]
	ROLES_DATA.Roles[0] = members.filter(member => member.get("team") === 0)
		.map(member => member.get("lane_selection_flags") as LaneSelectionFlags_t)
	ROLES_DATA.Roles[1] = members.filter(member => member.get("team") === 1)
		.map(member => member.get("lane_selection_flags") as LaneSelectionFlags_t)
})
