import { DOTA_GameMode, LaneSelectionFlags_t } from "wrapper/Imports"

export interface CDOTALobbyMember {
	id: bigint
	team: number
	slot: number
	lane_selection_flags: LaneSelectionFlags_t
}

export enum CSODOTALobby_State {
	UI = 0,
	READYUP = 4,
	SERVERSETUP = 1,
	RUN = 2,
	POSTGAME = 3,
	NOTREADY = 5,
	SERVERASSIGN = 6,
}

export interface CSODOTALobby {
	state: CSODOTALobby_State
	all_members: CDOTALobbyMember[]
	game_mode: DOTA_GameMode
	lobby_type: number
}

export class ROLES_DATA {
	public static Roles: Nullable<LaneSelectionFlags_t>[][] = [[], []]
	public static Dispose() {
		this.Roles[1] = this.Roles[0] = []
	}
}
