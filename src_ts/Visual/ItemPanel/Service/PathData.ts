export class PathDataX {

	public static readonly DOTAPathHeroesIMG = "panorama/images/heroes/"
	public static readonly DOTAPathHeroesIcon = PathDataX.DOTAPathHeroesIMG + "icons/"

	public static readonly DOTAPathItemIcon = "panorama/images/items/"
	public static readonly DOTAPathAbilityIcon = "panorama/images/spellicons/"

	public static readonly Units = (name: string, isBear: boolean = false, iconSmall: boolean = true) => {
		if (isBear)
			return `${PathDataX.Bear}_png.vtex_c`

		return iconSmall
			? `${PathDataX.DOTAPathHeroesIcon + name}_png.vtex_c`
			: `${PathDataX.DOTAPathHeroesIMG + name}_png.vtex_c`
	}

	public static readonly DOTAItems = (name: string | undefined) => {
		if (name !== undefined) {
			return `${PathDataX.DOTAPathItemIcon}${name && !name.includes("recipe_")
				? name.replace("item_", "")
				: "recipe"}_png.vtex_c`
		}
		return `${PathDataX.DOTAPathItemIcon}emptyitembg_png.vtex_c`
	}
	private static readonly Bear = PathDataX.DOTAPathHeroesIMG + "npc_dota_lone_druid_bear"

}
