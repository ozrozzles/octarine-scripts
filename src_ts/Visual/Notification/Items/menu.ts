import { Menu as MenuSDK } from "wrapper/Imports"

const arrItems: string[] = [
	"item_ward_observer",
	"item_ward_sentry",
	"item_ward_dispenser",
	"item_dust",
	"item_gem",
	"item_smoke_of_deceit",
]

const Menu = MenuSDK.AddEntryDeep(["Visual", "Notification Items"])

const ItemTpScrollTree = Menu.AddNode("TP Scroll")
export const TPState = ItemTpScrollTree.AddToggle("State", true)

const SettingsChatTree = ItemTpScrollTree.AddNode("Settings chat")
export const StateChatSend = SettingsChatTree.AddToggle("State", true, "send a chat to a message when a notification is clicked")
export const LanguageState = SettingsChatTree.AddDropdown("Language",
	["English", "Russian"], 0, "in which language to send notifications to chat on click",
)

export const TPVolume = ItemTpScrollTree.AddSlider("Volume", 2, 0, 100, 0, "sound volume as percents")

const PurchaseItemsTree = Menu.AddNode("Bought")
export const PurchaseItemState = PurchaseItemsTree.AddToggle("State", true)
export const PurchaseItemsCost = PurchaseItemsTree.AddSlider("Item cost", 2000, 500, 6000, 0, "Item cost threshold")
export const PurchaseItemsSelector = PurchaseItemsTree.AddImageSelector("Always show", arrItems, new Map(arrItems.map(name => [name, true])))
export const PurchaseItemsSoundVolume = PurchaseItemsTree.AddSlider("Volume", 2, 0, 100, 0, "sound volume as percents")

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Items", "Предметы"],
	["Bought", "Купленные"],
	["Volume", "Громкость"],
	["TP Scroll", "Телепорты"],
	["Item cost", "Стоимость предмета"],
	["Always show", "Оповещать всегда:"],
	["Notification Items", "Уведомление предметов"],
	["Item cost threshold", "Оповещать если цена предмета выше"],
	["send a chat to a message when a notification is clicked", "отправлять в сообщение чат при нажатии на уведомление"],
]))
