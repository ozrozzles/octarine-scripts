import { Building, GameSleeper, Hero, SpiritBear, Vector3 } from "../../../wrapper/Imports"

export class ITEMS_DATA {

	public static Sleeper = new GameSleeper()
	public static IgnoreItems: number[] = []
	public static Buildings: Building[] = []
	public static Units: (Hero | SpiritBear)[] = []
	public static TpScroll = new Map<number, Vector3>()

	public static Dispose() {
		this.Units = []
		this.Buildings = []
		this.IgnoreItems = []
		this.Sleeper.FullReset()
	}
}
