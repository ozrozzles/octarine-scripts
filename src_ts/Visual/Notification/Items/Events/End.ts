import { EventsX } from "X-Core/Imports"
import { ITEMS_DATA } from "../data"

EventsX.on("GameEnded", () => {
	ITEMS_DATA.Dispose()
})
