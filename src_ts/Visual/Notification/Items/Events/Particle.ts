import { EventsSDK, Hero, MapArea, NotificationsSDK, Vector3 } from "wrapper/Imports"
import { LocationX } from "X-Core/Imports"
import { ITEMS_DATA } from "../data"
import { TPState, TPVolume } from "../menu"
import { TPNotification } from "../Service/Notification"
import { INValidate } from "../Service/Validate"

EventsSDK.on("ParticleCreated", (id, path) => {
	if (!TPState.value || !INValidate.IsInGame || path.indexOf("teleport_end") === -1)
		return
	ITEMS_DATA.TpScroll.set(id, new Vector3())
})

EventsSDK.on("ParticleUpdated", (id, _, position) => {
	if (!TPState.value || !INValidate.IsInGame || position.LengthSqr <= 3)
		return
	const par = ITEMS_DATA.TpScroll.get(id)
	if (par === undefined)
		return
	ITEMS_DATA.TpScroll.set(id, position)
})

EventsSDK.on("ParticleUpdatedEnt", (id, _, entity) => {
	if (!TPState.value || !INValidate.IsInGame || !(entity instanceof Hero) || !entity.IsEnemy())
		return

	const position = ITEMS_DATA.TpScroll.get(id)
	if (position === undefined)
		return

	const LocationEnum = LocationX.GetLanePos(position)
	const building = ITEMS_DATA.Buildings.find(x => x.IsEnemy() && position.IsInRange(x.Position, 600))
	if (building === undefined && LocationEnum === MapArea.Unknown)
		return

	NotificationsSDK.Push(new TPNotification(
		entity,
		building === undefined ? LocationEnum : building,
		"sounds/ui/ping_warning.vsnd_c",
		(TPVolume.value / 100),
	))
})

EventsSDK.on("ParticleDestroyed", id => {
	ITEMS_DATA.TpScroll.delete(id)
})
