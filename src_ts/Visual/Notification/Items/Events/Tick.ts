import { EventsSDK, Item, NotificationsSDK } from "wrapper/Imports"
import { ITEMS_DATA } from "../data"
import { PurchaseItemsSoundVolume, PurchaseItemState } from "../menu"
import { ItemsNotificaton } from "../Service/Notification"
import { INValidate } from "../Service/Validate"

EventsSDK.on("Tick", () => {
	if (!PurchaseItemState.value || !INValidate.IsInGame)
		return
	ITEMS_DATA.Units.forEach(unit => {
		if (!unit.IsIllusion && unit.IsEnemy())
			unit.TotalItems.forEach(ItemNotificaton)
	})
})

function ItemNotificaton(item: Nullable<Item>) {
	if (item === undefined
		|| item.IsRecipe
		|| !INValidate.IsItemEnabled(item)
		|| ITEMS_DATA.IgnoreItems.indexOf(item.Index) !== -1)
		return

	NotificationsSDK.Push(new ItemsNotificaton(
		item,
		item.Owner,
		"sounds/ui/buy.vsnd_c",
		(PurchaseItemsSoundVolume.value / 100),
	))
	ITEMS_DATA.IgnoreItems.push(item.Index)
}
