import { ArrayExtensions, EventsSDK, Hero, Outpost, SpiritBear, Tower } from "wrapper/Imports"
import { ITEMS_DATA } from "../data"

EventsSDK.on("EntityCreated", ent => {

	if (ent instanceof Tower || ent instanceof Outpost)
		ITEMS_DATA.Buildings.push(ent)

	if (ent instanceof Hero || ent instanceof SpiritBear)
		ITEMS_DATA.Units.push(ent)
})

EventsSDK.on("EntityDestroyed", ent => {
	if (ent instanceof Tower || ent instanceof Outpost)
		ArrayExtensions.arrayRemove(ITEMS_DATA.Buildings, ent)

	if (ent instanceof Hero || ent instanceof SpiritBear)
		ArrayExtensions.arrayRemove(ITEMS_DATA.Units, ent)
})
