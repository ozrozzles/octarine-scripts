import { Building, Color, DOTAGameUIState_t, Entity, GameState, Hero, Item, item_travel_boots, item_travel_boots_2, LocalPlayer, MapArea, Notification, Outpost, Rectangle, RendererSDK, SpiritBear, Tower, Vector2 } from "wrapper/Imports"
import { LocationX, PathX } from "X-Core/Imports"
import { ChatService } from "../../_Service/Chat"
import { NotificationLanguage } from "../../_Service/Language"
import { LanguageState, StateChatSend } from "../menu"

class ClassPosition {
	public static Position(tower: Entity, str: string) {
		let str_position = ""
		let str_position_name = ""
		if (tower.Name.indexOf("tower1") !== -1)
			str_position = "T1"
		if (tower.Name.indexOf("tower2") !== -1)
			str_position = "T2"
		if (tower.Name.indexOf("tower3") !== -1)
			str_position = "T3"
		if (tower.Name.indexOf("tower4") !== -1)
			str_position = "T4"
		str_position_name = str
		return [str_position, str_position_name]
	}

	public static GetGoldPosition(position: Rectangle, heroPosition: Rectangle, itemPosition: Rectangle) {
		const result = position.Clone()
		result.Width = position.Width * 0.18
		result.Height = position.Height * 0.6
		result.x = (heroPosition.pos2.x + itemPosition.x - result.Width) / 2
		result.y += (position.Height - result.Height) / 2
		return result
	}

	public static GetHeroPosition(position: Rectangle) {
		const result = position.Clone()
		result.x += position.Width * 0.05
		result.y += position.Height * 0.15
		result.Width = position.Width * 0.3
		result.Height = position.Height * 0.7
		return result
	}

	public static GetItemPosition(position: Rectangle, heroPosition: Rectangle) {
		const result = heroPosition.Clone()
		result.Width *= 0.8
		result.x = position.pos2.x - position.Width * 0.05 - result.Width
		return result
	}

	public static CurrectionArrow(position: Rectangle) {
		const result = position.Clone()
		result.x += result.Height / 4
		result.y += result.Height / 4
		return result
	}
}

export class TPNotification extends Notification {
	constructor(private hero: Hero, private whereTo: Nullable<Building> | MapArea, sound: string, voulme: number) {
		super({
			playSound: sound,
			playVolume: voulme,
		})
	}

	public OnClick(): boolean {
		if (!StateChatSend.value)
			return false

		if (!(this.whereTo instanceof Building)) {
			ChatService.ExecuteTPOut(this.whereTo as MapArea, this.hero, LanguageState)
			return true
		}

		ChatService.ExecuteTPOut(LocationX.GetLanePos(this.whereTo.Position), this.hero, LanguageState)
		return true
	}

	public Draw(position: Rectangle): void {
		if (this.hero === undefined)
			return

		const opacity = this.Opacity
		const heroPosition = ClassPosition.GetHeroPosition(position)
		const itemPosition = ClassPosition.GetItemPosition(position, heroPosition)
		const goldPosition = ClassPosition.GetGoldPosition(position, heroPosition, itemPosition)
		const ArrowPosition = ClassPosition.CurrectionArrow(goldPosition)

		const opacityGray = Color.Gray.SetA(opacity)
		const opacityWhite = Color.White.SetA(opacity)

		RendererSDK.Image(PathX.Images.bg_deathsummary, position.pos1, -1, position.Size, opacityWhite)
		RendererSDK.Image(PathX.Heroes(this.hero.Name, false), heroPosition.pos1, -1, heroPosition.Size, opacityWhite)

		const travel = this.hero.GetItemByClass(item_travel_boots) ?? this.hero.GetItemByClass(item_travel_boots_2)
		const NameItemImage = travel === undefined ? "item_tpscroll" : travel.Name

		RendererSDK.Image(PathX.DOTAItems(NameItemImage), goldPosition.pos1, -1, goldPosition.Size, opacityWhite)
		RendererSDK.Image(PathX.Images.transfer_arrow_png, ArrowPosition.pos1, -1, ArrowPosition.Size, opacityWhite)

		let Image = ""
		let str_position = ""
		let str_position_name = "LOCATION"

		if (!(this.whereTo instanceof Entity)) {
			const areaPosition = LocationX.MapAreaLocationStr(this.whereTo as MapArea)

			if (this.whereTo === MapArea.DireBase || this.whereTo === MapArea.RadiantBase)
				Image = PathX.Images.tower_radiant

			this.TextRender(itemPosition, areaPosition.str_position, areaPosition.str_position_name, Image, opacityGray, opacityWhite)
			return
		}

		Image = !(this.whereTo instanceof Outpost)
			? PathX.Images.tower_radiant
			: PathX.Images.outpost

		if (this.whereTo instanceof Outpost) {
			if (this.whereTo.OutpostName.includes("North"))
				str_position = "TOP"

			if (this.whereTo.OutpostName.includes("South"))
				str_position = "BOT"

			str_position_name = "Outpost"
		}

		if (this.whereTo instanceof Tower) {
			if (this.whereTo.Name.includes("mid")) {
				const name = "MID"
				str_position = ClassPosition.Position(this.whereTo, name)[0]
				str_position_name = ClassPosition.Position(this.whereTo, name)[1]
			}

			if (this.whereTo.Name.includes("top")) {
				const name = "TOP"
				str_position = ClassPosition.Position(this.whereTo, name)[0]
				str_position_name = ClassPosition.Position(this.whereTo, name)[1]
			}

			if (this.whereTo.Name.includes("bot")) {
				const name = "BOT"
				str_position = ClassPosition.Position(this.whereTo, name)[0]
				str_position_name = ClassPosition.Position(this.whereTo, name)[1]
			}

			if (this.whereTo.Name.includes("tower4")) {
				str_position = "T4"
				str_position_name = "MID"
			}
		}

		this.TextRender(itemPosition, str_position, str_position_name, Image, opacityGray, opacityWhite)
	}

	private TextRender(itemPosition: Rectangle, str_position: string, str_position_name: string, Image: string, opacityGray: Color, opacityWhite: Color) {
		const [position, position_name] = NotificationLanguage.TranslatePosition(str_position, str_position_name)

		if (Image !== "")
			RendererSDK.Image(Image, itemPosition.pos1, -1, itemPosition.Size, opacityGray)

		const vec = new Vector2(itemPosition.x, itemPosition.y + (itemPosition.Width / 6))
			.AddScalarX((itemPosition.Width - RendererSDK.GetTextSize(position,
				"Calibri", itemPosition.Height / 2.5).x) / 2)

		const vec2 = new Vector2(itemPosition.x, itemPosition.y + (itemPosition.Width / 2.15))
			.AddScalarX((itemPosition.Width - RendererSDK.GetTextSize(position_name,
				"Calibri", itemPosition.Height / 3).x) / 2)

		RendererSDK.Text(position, vec, opacityWhite, "Calibri", itemPosition.Height / 2.5)
		RendererSDK.Text(position_name, vec2, opacityWhite, "Calibri", itemPosition.Height / 3)
	}

}

export class ItemsNotificaton extends Notification {
	constructor(private item: Item, private unit: Nullable<(Hero | SpiritBear)>, sound: string, volume: number) {
		super({
			playSound: sound,
			playVolume: volume,
		})
	}

	public OnClick(): boolean {
		LocalPlayer!.Hero!.PingAbility(this.item)
		return true
	}

	public Draw(position: Rectangle): void {
		const owner = this.unit
		if (owner === undefined || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
			return
		const heroPosition = ClassPosition.GetHeroPosition(position)
		const itemPosition = ClassPosition.GetItemPosition(position, heroPosition)
		const goldPosition = ClassPosition.GetGoldPosition(position, heroPosition, itemPosition)
		const opacity = Color.White.SetA(this.Opacity)

		let ImageHero = PathX.Heroes(owner.Name, false)
		if (owner instanceof SpiritBear)
			ImageHero = PathX.DOTAPathHeroes + `npc_dota_lone_druid_bear_png.vtex_c`

		RendererSDK.Image(PathX.Images.bg_deathsummary, position.pos1, -1, position.Size, opacity)
		RendererSDK.Image(ImageHero, heroPosition.pos1, -1, heroPosition.Size, opacity)
		RendererSDK.Image(PathX.Images.gold_large, goldPosition.pos1, -1, goldPosition.Size, opacity)
		RendererSDK.Image(PathX.DOTAItems(this.item.Name), itemPosition.pos1, -1, itemPosition.Size, opacity)
	}
}
