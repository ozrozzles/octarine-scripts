import { GameRules, GameState, Item, LocalPlayer, PlayerResource, Team } from "wrapper/Imports"
import { PurchaseItemsCost, PurchaseItemsSelector } from "../menu"

export class INValidate {
	public static get IsInGame() {
		return GameRules !== undefined
			&& GameRules.IsInGame
			&& PlayerResource !== undefined
			&& LocalPlayer !== undefined
			&& GameState.LocalTeam !== Team.Observer
	}

	public static IsItemEnabled = (item: Item) => {
		if (PurchaseItemsSelector.IsEnabled(item.Name))
			return true
		return item.Cost >= PurchaseItemsCost.value
	}
}
