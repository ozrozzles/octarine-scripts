import { ABILITIES_DATA } from "../data"

export class ABILITY_UTILITY {

	public static MtRand(min: number, max: number) {
		return Math.round(Math.random() * (max - min) + min)
	}

	public static VSND_INIT() {
		for (let index = 1; index < 8; index++)
			ABILITIES_DATA.MiranaVsnd.push(`sounds/vo/mirana/mir_ability_moon_0${index}.vsnd_c`)

		for (let index = 1; index < 9; index++) {
			ABILITIES_DATA.NyxVsnd.push(`sounds/vo/nyx_assassin/nyx_vendetta_0${index}.vsnd_c`)
			ABILITIES_DATA.AAVsnd.push(`sounds/vo/ancient_apparition/appa_ability_iceblast_0${index}.vsnd_c`)
		}
		for (let index = 1; index < 10; index++) {
			const elem = index <= 9 ? "0" : ""
			ABILITIES_DATA.Lycan.push(`sounds/vo/lycan/lycan_ability_shapeshift_${elem}${index}.vsnd_c`)
		}
		for (let index = 1; index < 19; index++) {
			const elem = index <= 9 ? "0" : ""
			ABILITIES_DATA.Breaker.push(`sounds/vo/spirit_breaker/spir_ability_charge_${elem}${index}.vsnd_c`)
		}
	}
}
