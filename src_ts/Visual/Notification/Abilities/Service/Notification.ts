import { Ability, Color, Creep, furion_teleportation, Hero, LocalPlayer, MapArea, Notification, Rectangle, RendererSDK, Team, Unit, Vector2, Vector3 } from "wrapper/Imports"
import { LocationX, PathX } from "X-Core/Imports"
import { ChatService } from "../../_Service/Chat"
import { NotificationLanguage } from "../../_Service/Language"
import { LanguageState, StateChatSend } from "../menu"

export class AbilitiesNotification extends Notification {
	private target_area = MapArea.Unknown
	constructor(
		private hero: Hero,
		private abilities: Ability,
		sound: string,
		volume: number,
		private target?: Vector3 | Unit,
	) {
		super({
			playSound: sound,
			playVolume: volume,
			timeToShow: 3 * 1000,
			uniqueKey: `AbilitiesNotification_${hero.Index}_${abilities.Index}`,
		})
		if (this.target instanceof Vector3)
			this.target_area = LocationX.GetLanePos(this.target)
	}

	public OnClick(): boolean {
		if (LocalPlayer === undefined || LocalPlayer.Hero === undefined || !StateChatSend.value)
			return false

		if (this.abilities instanceof furion_teleportation && this.target instanceof Vector3) {
			ChatService.ExecuteTPOut(LocationX.GetLanePos(this.target), this.hero, LanguageState)
			return true
		}

		LocalPlayer.Hero.PingAbility(this.abilities)
		return true
	}

	public Draw(position: Rectangle): void {
		const heroPosition = this.GetHeroPosition(position)
		const opacityWhite = Color.White.SetA(this.Opacity)
		const InfoPosition = this.GetInfoPosition(position, heroPosition)
		const centerPosition = this.GetCenterPosition(position, heroPosition, InfoPosition)
		const ArrowPosition = this.CurrectionArrow(centerPosition)

		RendererSDK.Image(PathX.Images.bg_deathsummary, position.pos1, -1, position.Size, opacityWhite)
		RendererSDK.Image(PathX.Heroes(this.hero.Name, false), heroPosition.pos1, -1, heroPosition.Size, opacityWhite)

		if (this.target !== undefined) {
			RendererSDK.Image(PathX.DOTAAbilities(this.abilities.Name), centerPosition.pos1, -1, centerPosition.Size, opacityWhite)
			RendererSDK.Image(PathX.Images.transfer_arrow_png, ArrowPosition.pos1, -1, ArrowPosition.Size, opacityWhite)

			if (this.target instanceof Hero || this.target instanceof Creep) {
				let image = ""

				if (this.target instanceof Hero)
					image = PathX.Heroes(this.target.Name, false)

				if (this.target instanceof Creep)
					image = PathX.Heroes(this.hero.Team === Team.Dire
						? "npc_dota_hero_creep_radiant"
						: "npc_dota_hero_creep_dire", false)

				RendererSDK.Image(image, InfoPosition.pos1, -1, InfoPosition.Size, opacityWhite)
				return
			}

			if (this.target instanceof Vector3) {
				const Location = LocationX.MapAreaLocationStr(this.target_area)
				const [str_position, str_position_name] =
					NotificationLanguage.TranslatePosition(Location.str_position, Location.str_position_name)

				const vec = new Vector2(InfoPosition.x, InfoPosition.y + (InfoPosition.Width / 6))
					.AddScalarX((InfoPosition.Width - RendererSDK.GetTextSize(str_position,
						"Calibri", InfoPosition.Height / 2.5).x) / 2)

				const vec2 = new Vector2(InfoPosition.x, InfoPosition.y + (InfoPosition.Width / 2.15))
					.AddScalarX((InfoPosition.Width - RendererSDK.GetTextSize(str_position_name,
						"Calibri", InfoPosition.Height / 3).x) / 2)

				RendererSDK.Text(str_position, vec, opacityWhite, "Calibri", InfoPosition.Height / 2.5)
				RendererSDK.Text(str_position_name, vec2, opacityWhite, "Calibri", InfoPosition.Height / 3)
			}

			return
		}

		RendererSDK.Image(PathX.Images.transfer_arrow_png, centerPosition.pos1, -1, centerPosition.Size, opacityWhite)
		RendererSDK.Image(PathX.DOTAAbilities(this.abilities.Name), InfoPosition.pos1, -1, InfoPosition.Size, opacityWhite)
	}

	private GetHeroPosition(position: Rectangle) {
		const result = position.Clone()
		result.x += position.Width * 0.05
		result.y += position.Height * 0.15
		result.Width = position.Width * 0.3
		result.Height = position.Height * 0.7
		return result
	}

	private GetCenterPosition(position: Rectangle, heroPosition: Rectangle, itemPosition: Rectangle) {
		const result = position.Clone()
		result.Width = position.Width * 0.18
		result.Height = position.Height * 0.6
		result.x = (heroPosition.pos2.x + itemPosition.x - result.Width) / 2
		result.y += (position.Height - result.Height) / 2
		return result
	}

	private GetInfoPosition(position: Rectangle, heroPosition: Rectangle) {
		const result = heroPosition.Clone()
		result.Width *= 0.8
		result.x = position.pos2.x - position.Width * 0.05 - result.Width
		return result
	}

	private CurrectionArrow(position: Rectangle) {
		const result = position.Clone()
		result.x = position.x + result.Height / 4
		result.y = position.y + result.Height / 4
		return result
	}
}
