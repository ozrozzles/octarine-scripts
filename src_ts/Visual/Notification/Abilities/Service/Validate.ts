import { Ability, Entity, EntityManager, GameRules, GameState, Hero, LocalPlayer, Team } from "wrapper/Imports"

export class ANValidate {
	public static get IsInGame() {
		return GameRules !== undefined
			&& GameRules.IsInGame
			&& LocalPlayer !== undefined
			&& GameState.LocalTeam !== Team.Observer
	}

	public static EntityVerificaton(unit: Entity | string, abilities: Ability | string) {
		let hero: Nullable<Hero>
		let abil: Nullable<Ability>

		if (typeof unit === "string")
			hero = EntityManager.GetEntitiesByClass(Hero).find(x => !x.IsIllusion && x.Name === unit)

		if (unit instanceof Hero)
			hero = unit

		if (hero === undefined || !hero.IsEnemy())
			return undefined

		if (typeof abilities === "string")
			abil = hero.GetAbilityByName(abilities)

		if (abilities instanceof Ability)
			abil = abilities

		if (abil === undefined || !abil.Owner!.IsEnemy())
			return undefined

		return { Ability: abil, Hero: hero }
	}
}
