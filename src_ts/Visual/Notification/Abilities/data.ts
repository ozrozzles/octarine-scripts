import { GameSleeper, Unit } from "wrapper/Imports"

export class ABILITIES_DATA {
	public static BaseNPC: Nullable<Unit>
	public static AAVsnd: string[] = []
	public static NyxVsnd: string[] = []
	public static Breaker: string[] = []
	public static Lycan: string[] = []
	public static MiranaVsnd: string[] = []
	public static Sleeper = new GameSleeper()
	public static ParticlesMap = new Map<number, [string, string]>()

	public static Particles: string[] = [
		"particles/units/heroes/hero_mirana/mirana_moonlight_recipient.vpcf",
		"particles/units/heroes/hero_nyx_assassin/nyx_assassin_vendetta_start.vpcf",
		"particles/units/heroes/hero_nevermore/nevermore_wings.vpcf",
		"particles/units/heroes/hero_dark_willow/dark_willow_wisp_spell_channel.vpcf",
		"particles/units/heroes/hero_furion/furion_teleport_end.vpcf",
		"particles/units/heroes/hero_lycan/lycan_shapeshift_cast.vpcf",
	]

	public static ParticlesVerificaton: string[] = [
		"furion_teleportation",
		"dark_willow_terrorize",
		"nyx_assassin_vendetta",
		"nevermore_requiem",
		"mirana_invis",
		"lycan_shapeshift",
	]

	public static Dispose() {
		this.BaseNPC = undefined
		this.Sleeper.FullReset()
		this.ParticlesMap.clear()
	}
}
