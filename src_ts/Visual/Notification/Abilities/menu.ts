import { Menu as MenuSDK } from "wrapper/Imports"

const Menu = MenuSDK.AddEntryDeep(["Visual", "Notification Abilities"])
export const AbilitiesState = Menu.AddToggle("State", true)

const IsEnableAbility: string[] = [
	"nyx_assassin_vendetta",
	"dark_willow_terrorize",
	"nevermore_requiem",
	"mirana_invis",
	"invoker_sun_strike",
	"spirit_breaker_charge_of_darkness",
	"ancient_apparition_ice_blast",
	"furion_teleportation",
	"wisp_relocate",
	"lycan_shapeshift",
]

export const AbilitiesIsEnebled = Menu.AddImageSelector(
	"Abilities",
	IsEnableAbility,
	new Map(IsEnableAbility.map(name => [name, true])),
)

const SettingsChatTree = Menu.AddNode("Settings chat")
export const StateChatSend = SettingsChatTree.AddToggle("State", true, "send a chat to a message when a notification is clicked")
export const LanguageState = SettingsChatTree.AddDropdown("Language",
	["English", "Russian"], 0, "in which language to send notifications to chat on click",
)

const SettingsSound = Menu.AddNode("Settings sound")
export const AbilitiesVO = SettingsSound.AddToggle(
	"VO Announcer",
	true,
	"Enable supported announcer for hero",
)

export const AbilitiesVolume = SettingsSound.AddSlider(
	"Volume", 2, 0, 100, 0,
	"sound volume as percents",
)

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Abilities", "Способности"],
	["Settings chat", "Настройки чата"],
	["Settings sound", "Настройки звука"],
	["Notification Abilities", "Уведомление способностей"],
	["in which language to send notifications to chat on click", "на каком языке отправлять в чат уведомления при клике"],
]))
