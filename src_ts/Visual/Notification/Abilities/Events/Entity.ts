import { EventsSDK, Unit } from "wrapper/Imports"
import { ABILITIES_DATA } from "../data"

EventsSDK.on("EntityCreated", ent => {
	if (!(ent instanceof Unit)
		|| ent.ClassName !== "CDOTA_BaseNPC"
		|| ent.ModelName !== "models/development/invisiblebox.vmdl")
		return
	ABILITIES_DATA.BaseNPC = ent
})

EventsSDK.on("EntityDestroyed", ent => {
	if (!(ent instanceof Unit)
		|| ent.ClassName !== "CDOTA_BaseNPC"
		|| ent.ModelName !== "models/development/invisiblebox.vmdl")
		return
	ABILITIES_DATA.BaseNPC = undefined
})
