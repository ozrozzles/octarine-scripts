import { Entity, EventsSDK, lycan_shapeshift, mirana_invis, NotificationsSDK, nyx_assassin_vendetta } from "wrapper/Imports"
import { ABILITIES_DATA } from "../data"
import { AbilitiesIsEnebled, AbilitiesState, AbilitiesVO, AbilitiesVolume } from "../menu"
import { AbilitiesNotification } from "../Service/Notification"
import { ABILITY_UTILITY } from "../Service/Utility"
import { ANValidate } from "../Service/Validate"

ABILITY_UTILITY.VSND_INIT()
const PING_SOUND = "sounds/ui/ping_warning.vsnd_c"

EventsSDK.on("ParticleCreated", (id, path, __, ___, target) => {
	if (ABILITIES_DATA.Particles.indexOf(path) === -1 || !AbilitiesState.value || !ANValidate.IsInGame)
		return

	const [abil_furion, abil_willow, abil_vendetta, abil_requiem, abil_mirana, abil_shapeshift] = ABILITIES_DATA.ParticlesVerificaton

	if (path.indexOf("furion_teleport_end") !== -1 && AbilitiesIsEnebled.IsEnabled(abil_furion)) {
		ABILITIES_DATA.ParticlesMap.set(id, ["npc_dota_hero_furion", abil_furion])
		return
	}

	let str_ability = ""
	let vo_play = PING_SOUND
	let sound_volume = (AbilitiesVolume.value / 100)

	let str_entity: string | Entity = target === undefined ? "" : target as Entity

	if (path.indexOf("dark_willow_wisp_spell_channel") !== -1 && AbilitiesIsEnebled.IsEnabled(abil_willow)) {
		str_entity = "npc_dota_hero_dark_willow"
		str_ability = abil_willow
	}

	if (path.indexOf("lycan_shapeshift_cast") !== -1 && AbilitiesIsEnebled.IsEnabled(abil_shapeshift)) {
		str_ability = abil_shapeshift

		if (AbilitiesVO.value)
			vo_play = ABILITIES_DATA.Lycan[ABILITY_UTILITY.MtRand(0, 9)]
	}

	if (path.indexOf("nyx_assassin_vendetta_start") !== -1 && AbilitiesIsEnebled.IsEnabled(abil_vendetta)) {
		str_entity = "npc_dota_hero_nyx_assassin"
		str_ability = abil_vendetta

		if (AbilitiesVO.value)
			vo_play = ABILITIES_DATA.NyxVsnd[ABILITY_UTILITY.MtRand(0, 8)]
	}

	if (path.indexOf("nevermore_wings") !== -1 && AbilitiesIsEnebled.IsEnabled(abil_requiem))
		str_ability = abil_requiem

	if (path.indexOf("mirana_moonlight_recipient") !== -1 && AbilitiesIsEnebled.IsEnabled(abil_mirana)) {
		str_ability = abil_mirana

		if (AbilitiesVO.value)
			vo_play = ABILITIES_DATA.MiranaVsnd[ABILITY_UTILITY.MtRand(0, 7)]
	}

	const verificaton = ANValidate.EntityVerificaton(str_entity, str_ability)

	if (verificaton === undefined)
		return

	if (verificaton.Ability instanceof mirana_invis
		|| verificaton.Ability instanceof lycan_shapeshift
		|| verificaton.Ability instanceof nyx_assassin_vendetta) {
		if (AbilitiesVO.value && sound_volume !== 0)
			sound_volume = + 0.15
	}

	NotificationsSDK.Push(
		new AbilitiesNotification(
			verificaton.Hero,
			verificaton.Ability,
			vo_play,
			sound_volume,
		),
		true,
	)
})

EventsSDK.on("ParticleUpdated", (id, controlPoint, position) => {
	if (!AbilitiesState.value || !ANValidate.IsInGame)
		return
	const particle = ABILITIES_DATA.ParticlesMap.get(id)
	if (particle === undefined || controlPoint !== 1)
		return

	const verificaton = ANValidate.EntityVerificaton(particle[0], particle[1])

	if (verificaton === undefined)
		return

	NotificationsSDK.Push(
		new AbilitiesNotification(
			verificaton.Hero,
			verificaton.Ability,
			PING_SOUND,
			AbilitiesVolume.value / 100,
			position,
		),
		true,
	)
})

EventsSDK.on("ParticleDestroyed", id =>
	ABILITIES_DATA.ParticlesMap.delete(id))
