import { EventsSDK, Hero, Modifier, NotificationsSDK } from "wrapper/Imports"
import { ABILITIES_DATA } from "../data"
import { AbilitiesIsEnebled, AbilitiesState, AbilitiesVO, AbilitiesVolume } from "../menu"
import { AbilitiesNotification } from "../Service/Notification"
import { ABILITY_UTILITY } from "../Service/Utility"
import { ANValidate } from "../Service/Validate"

function CreateNotifiaction(name: string, m0d: Modifier) {
	if (m0d.Name !== name)
		return

	const caster = m0d.Caster
	const target = m0d.Parent
	const abililty = m0d.Ability
	let VO_SOUND = "sounds/ui/ping_warning.vsnd_c"

	if (caster === undefined || target === undefined || abililty === undefined)
		return

	let sound_volume = (AbilitiesVolume.value / 100)
	const verificaton = ANValidate.EntityVerificaton(caster, abililty)

	if (verificaton === undefined)
		return

	if (!(caster instanceof Hero) || !AbilitiesIsEnebled.IsEnabled(abililty.Name))
		return

	if (AbilitiesVO.value)
		VO_SOUND = ABILITIES_DATA.Breaker[ABILITY_UTILITY.MtRand(0, 17)]

	if (AbilitiesVO.value && sound_volume !== 0)
		sound_volume = + 0.15

	NotificationsSDK.Push(new AbilitiesNotification(caster,
		abililty,
		name === "modifier_wisp_relocate_thinker" ? "sounds/ui/ping_warning.vsnd_c" : VO_SOUND,
		sound_volume,
		name === "modifier_wisp_relocate_thinker" ? target.Position : target,
	))
}

EventsSDK.on("ModifierCreated", m0d => {
	if (!ANValidate.IsInGame || !AbilitiesState.value)
		return
	CreateNotifiaction("modifier_wisp_relocate_thinker", m0d)
	CreateNotifiaction("modifier_spirit_breaker_charge_of_darkness_vision", m0d)
})
