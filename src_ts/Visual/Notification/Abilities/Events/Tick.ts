import { EventsSDK, NotificationsSDK, npc_dota_hero_invoker, Unit } from "wrapper/Imports"
import { ABILITIES_DATA } from "../data"
import { AbilitiesIsEnebled, AbilitiesState, AbilitiesVO, AbilitiesVolume } from "../menu"
import { AbilitiesNotification } from "../Service/Notification"
import { ABILITY_UTILITY } from "../Service/Utility"
import { ANValidate } from "../Service/Validate"

let sounds_vo_play = "sounds/ui/ping_warning.vsnd_c"

function IceBlast(unit: Nullable<Unit>) {

	const abil_ = "ancient_apparition_ice_blast"
	const hero = "npc_dota_hero_ancient_apparition"

	if (!AbilitiesIsEnebled.IsEnabled(abil_)
		|| !(unit instanceof Unit)
		|| !unit.IsValid
		|| !unit.IsEnemy()
		|| unit.UnitStateMask !== 8619311488n
		|| !unit.UnitState.some(x => x === 16777216)
		|| ABILITIES_DATA.Sleeper.Sleeping(`notification_IceBlast_${unit.Index}`))
		return

	let volume = (AbilitiesVolume.value / 100)
	const verificaton_ = ANValidate.EntityVerificaton(hero, abil_)

	if (verificaton_ === undefined)
		return

	if (AbilitiesVO.value) {
		sounds_vo_play = ABILITIES_DATA.AAVsnd[ABILITY_UTILITY.MtRand(0, 8)]
		volume = + 0.15
	}

	NotificationsSDK.Push(new AbilitiesNotification(
		verificaton_.Hero,
		verificaton_.Ability,
		sounds_vo_play,
		volume,
	))
	ABILITIES_DATA.Sleeper.Sleep(15000, `notification_IceBlast_${unit.Index}`)
}

function SunStrike(unit: Nullable<Unit>) {

	const abil = "invoker_sun_strike"

	if (!AbilitiesIsEnebled.IsEnabled(abil)
		|| unit === undefined
		|| unit.Name !== "npc_dota_thinker"
		|| !(unit.OwnerEntity instanceof npc_dota_hero_invoker)
		|| ABILITIES_DATA.Sleeper.Sleeping(`notification_sun_strike`))
		return

	const verificaton = ANValidate.EntityVerificaton(unit.OwnerEntity, abil)

	if (verificaton === undefined)
		return

	NotificationsSDK.Push(new AbilitiesNotification(
		verificaton.Hero,
		verificaton.Ability,
		sounds_vo_play,
		AbilitiesVolume.value / 100,
	))
	ABILITIES_DATA.Sleeper.Sleep(3500, `notification_sun_strike`)
}

EventsSDK.on("Tick", () => {
	if (!ANValidate.IsInGame || !AbilitiesState.value)
		return
	const BaseNPC = ABILITIES_DATA.BaseNPC
	IceBlast(BaseNPC)
	SunStrike(BaseNPC)
})
