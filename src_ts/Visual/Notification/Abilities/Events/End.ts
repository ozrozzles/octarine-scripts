import { EventsX } from "X-Core/Imports"
import { ABILITIES_DATA } from "../data"

EventsX.on("GameEnded", () => {
	ABILITIES_DATA.Dispose()
})
