import { GameState, Hero, LocalPlayer, MapArea, Menu, Team } from "wrapper/Imports"
import { AbbX } from "X-Core/Imports"

interface LocationMessage {
	Hero: Hero,
	locationEnum: MapArea
	MenuLanguage: Menu.Dropdown
	locationEnumSelected: MapArea,
	Language: { RU: string, EN: string }
}

export class ChatService {

	public static ExecuteTPOut(location: MapArea, hero: Hero, menuLangs: Menu.Dropdown) {

		const Player = LocalPlayer!.Hero

		if (Player === undefined)
			return

		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.Top,
			Language: { RU: "тп топ", EN: "tp top" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.Middle,
			Language: { RU: "тп мид", EN: "tp mid" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.Bottom,
			Language: { RU: "тп бот", EN: "tp bot" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.Top,
			Language: { RU: "тп топ", EN: "tp top" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.Bottom,
			Language: { RU: "тп бот", EN: "tp bot" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.RadiantBase,
			Language: this.TPTranslateBase(hero, Team.Radiant),
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.DireBase,
			Language: this.TPTranslateBase(hero, Team.Dire),
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.RoshanPit,
			Language: { RU: "тп рошан", EN: "tp roshan" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.RiverTop,
			Language: { RU: "тп речка топ", EN: "tp river top" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.RiverMiddle,
			Language: { RU: "тп речка мид", EN: "tp river mid" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.RiverBottom,
			Language: { RU: "тп речка бот", EN: "tp river bot" },
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.DireBottomJungle,
			Language: this.TPTranslateJungleBOT(hero, Team.Dire),
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.DireTopJungle,
			Language: this.TPTranslateJungleTOP(hero, Team.Dire),
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.RadiantBottomJungle,
			Language: this.TPTranslateJungleBOT(hero, Team.Radiant),
		})
		this.SendMessage({
			Hero: hero,
			locationEnum: location,
			MenuLanguage: menuLangs,
			locationEnumSelected: MapArea.RadiantBottomJungle,
			Language: this.TPTranslateJungleTOP(hero, Team.Radiant),
		})

	}

	private static LanguageSettings(menu: Menu.Dropdown) {
		return menu.selected_id !== 0
	}

	private static SendMessage(option: LocationMessage) {
		if (option.locationEnum !== option.locationEnumSelected)
			return
		const Language = this.LanguageSettings(option.MenuLanguage)
		this.Message(Language ? option.Language.RU : option.Language.EN, option.MenuLanguage, option.Hero)
	}

	private static TPTranslateBase(hero: Hero, team: Team) {
		return {
			RU: hero.Team === team ? "тп на базу" : "тп к нам на базу",
			EN: hero.Team === team ? "TP to base" : "TP to our base",
		}
	}

	private static TPTranslateJungleTOP(hero: Hero, team: Team) {
		return {
			RU: hero.Team === team ? "тп к себе в лес на топ" : "тп к нам в лес на топ",
			EN: hero.Team === team ? "TP to my forest on top" : "TP to us in the woods on top",
		}
	}

	private static TPTranslateJungleBOT(hero: Hero, team: Team) {
		return {
			RU: hero.Team === team ? "тп к себе в лес на бот" : "тп к нам в лес на бот",
			EN: hero.Team === team ? "TP to your forest to the bot" : "TP to our forest to the bot",
		}
	}

	private static Message(message: string, menu: Menu.Dropdown, hero: Hero) {
		const name = AbbX.NAME_HERO(hero, this.LanguageSettings(menu))
		GameState.ExecuteCommand(`say_team ${name} ${message}`)
	}
}
