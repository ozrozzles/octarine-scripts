import { Menu } from "wrapper/Imports"

export class NotificationLanguage {

	public static TranslatePosition(position: string, position_name: string) {
		const Language = Menu.Localization.SelectedUnitName === "russian"

		if (position === "BASE")
			position = Language ? "БАЗА" : "BASE"

		if (position === "River TOP")
			position = Language ? "Река ТОП" : "River TOP"

		if (position === "River MID")
			position = Language ? "Река МИД" : "River MID"

		if (position === "River BOT")
			position = Language ? "Река БОТ" : "River BOT"

		if (position === "RIVER")
			position = Language ? "Река" : "River"

		if (position === "TOP")
			position = Language ? "ТОП" : "TOP"

		if (position === "MID")
			position = Language ? "МИД" : "MID"

		if (position_name === "Dire")
			position_name = Language ? "Тьма" : "Dire"

		if (position_name === "Radiant")
			position_name = Language ? "Свет" : "Radiant"

		if (position === "BOT")
			position = Language ? "БОТ" : "BOT"

		if (position_name === "TOP")
			position_name = Language ? "ТОП" : "TOP"

		if (position_name === "MID")
			position_name = Language ? "МИД" : "MID"

		if (position_name === "BOT")
			position_name = Language ? "БОТ" : "BOT"

		if (position === "JUNGLE")
			position = Language ? "ЛЕС" : "JUNGLE"

		if (position === "ROSHAN")
			position = Language ? "РОШАН" : "ROSHAN"

		if (position_name === "BOT Radiant")
			position_name = Language ? "БОТ Свет" : "BOT Radiant"

		if (position_name === "BOT Dire")
			position_name = Language ? "БОТ Тьма" : "BOT Dire"

		if (position_name === "TOP Dire")
			position_name = Language ? "ТОП Тьма" : "TOP Dire"

		if (position_name === "LOCATION")
			position_name = Language ? "ЛОКАЦИЯ" : "LOCATION"

		if (position_name === "TOP Radiant")
			position_name = Language ? "ТОП Свет" : "TOP Radiant"

		return [position, position_name]
	}
}
