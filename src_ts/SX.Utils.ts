import { Color, DotaMap, EventsSDK, LocalPlayer, ParticlesSDK } from "./wrapper/Imports"

const dotaMap = new DotaMap()
const particleSDK = new ParticlesSDK()

EventsSDK.on("Draw", () => {
	const player = LocalPlayer?.Hero
	if (!(globalThis as any).DOTA_MAP || player === undefined)
		return

	dotaMap.Roshan.Draw("Roshan", player, particleSDK, Color.Red, 25, 25)
	dotaMap.DireBase.Draw("DireBase", player, particleSDK, Color.Aqua, 25, 25)
	dotaMap.RadiantBase.Draw("RadiantBase", player, particleSDK, Color.Aqua, 25, 25)

	dotaMap.Top.Draw("Top", player, particleSDK, Color.Aqua, 25, 25)
	dotaMap.Middle.Draw("Middle", player, particleSDK, Color.Red, 25, 25)
	dotaMap.Bottom.Draw("Bottom", player, particleSDK, Color.Aqua, 25, 25)

	dotaMap.RiverTop.Draw("RiverTop", player, particleSDK, Color.Aqua, 25, 25)
	dotaMap.RiverMiddle.Draw("RiverMiddle", player, particleSDK, Color.Aqua, 25, 25)
	dotaMap.RiverBottom.Draw("RiverBottom", player, particleSDK, Color.Aqua, 25, 25)

	dotaMap.RadiantTopJungle.Draw("RadiantTopJungle", player, particleSDK, Color.Fuchsia, 25, 25)
	dotaMap.DireTopJungle.Draw("DireTopJungle", player, particleSDK, Color.Fuchsia, 25, 25)

	dotaMap.RadiantBottomJungle.Draw("RadiantBottomJungle", player, particleSDK, Color.Fuchsia, 25, 25)
	dotaMap.DireBottomJungle.Draw("DireBottomJungle", player, particleSDK, Color.Fuchsia, 25, 25)
})
