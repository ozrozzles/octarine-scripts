import { Events } from "wrapper/Imports"
import { MainMenuMapName } from "../menu"

const clear_list: string[] = []
Events.on("AddSearchPath", path => {
	if (MainMenuMapName.selected_id !== 0 && path.endsWith("dota.vpk")) {
		AddSearchPath(path)
		const new_path = path.substring(0, path.length - 8) + MainMenuMapName.InternalValuesNames[MainMenuMapName.selected_id] + ".vpk"
		AddSearchPath(new_path)
		clear_list.push(new_path)
		return false
	}
	if (MainMenuMapName.InternalValuesNames.some((name, i) => i !== 0 && path.endsWith(name + ".vpk")))
		return false
	return true
})

Events.on("PostRemoveSearchPath", path => {
	if (path.endsWith("dota.vpk"))
		clear_list.forEach(path_ => RemoveSearchPath(path_))
})
