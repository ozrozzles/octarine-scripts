import { CreateMenuHeroSelection } from "../menu"
const Lane: string[] = ["None", "Hard", "Mid", "Easy", "Jungle", "Enemy jungle"]
export const AutoLaneMarker = CreateMenuHeroSelection("Utility", "Auto Lane marker")
export const LaneMarkerState = AutoLaneMarker.Menu.AddDropdown("Select lane", Lane)
