import { ArrayExtensions, EventsSDK, LocalPlayer, Menu, TickSleeper, Unit } from "wrapper/Imports"

const menu = Menu.AddEntryDeep(["Utility", "Mine Destroyer"])
const menuState = menu.AddToggle("State", true)

const mines: Unit[] = [] // C_DOTA_NPC_TechiesMines

EventsSDK.on("EntityCreated", ent => {
	if (ent.ClassName === "CDOTA_NPC_TechiesMines")
		mines.push(ent as Unit)
})

EventsSDK.on("EntityDestroyed", ent => {
	if (ent.ClassName === "CDOTA_NPC_TechiesMines")
		ArrayExtensions.arrayRemove(mines, ent)
})

const AttackSleeper = new TickSleeper()
EventsSDK.on("Tick", () => {
	if (!menuState.value || AttackSleeper.Sleeping)
		return

	const hero = LocalPlayer!.Hero

	if (hero === undefined || !hero.IsAlive || hero.IsChanneling || hero.IsInFadeTime)
		return

	const mine = mines.find(mine_ =>
		mine_.IsEnemy()
		&& mine_.IsAlive
		&& hero.CanAttack(mine_)
		&& mine_.IsInRange(hero, hero.AttackRange)
		&& mine_.Name === "npc_dota_techies_land_mine",
	)
	if (mine !== undefined) {
		hero.AttackTarget(mine)
		AttackSleeper.Sleep(100)
	}
})

EventsSDK.on("GameEnded", () => AttackSleeper.ResetTimer())
